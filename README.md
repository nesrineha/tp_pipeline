# TP_pipeline

## terraform commandes
apply --             Builds or changes infrastructure
console --           Interactive console for Terraform interpolations
destroy   --         Destroy Terraform-managed infrastructure
env  --              Workspace management
fmt  --            Rewrites config files to canonical format
get  --            Download and install modules for the configuration
graph  --            Create a visual graph of Terraform resources
import  --          Import existing infrastructure into Terraform
init    --         Initialize a Terraform working directory
output             Read an output from a state file
plan               Generate and show an execution plan
providers          Prints a tree of the providers used in the configuration
refresh            Update local state file against real resources
show               Inspect Terraform state or plan

## kinesis application
Pour créer une application Kinesis nous avons besoin :

kinesis stream
S3 bucket
kinesis firehose à S3
lambda pour copier depuis les stream à firehose
Fichier de log pour les erreurs logging de firehose


## Schéma d'infrastructure
![Schema](/schema-Infrastructure.png)

## schéma flux de données
![Schema](/schema-flux.png)