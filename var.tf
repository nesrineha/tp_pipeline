variable "aws_var_region" {
  default  = "eu-west-1"
}

variable "vpc_var_cidr" {
    default = "10.0.0.0/16"
}


variable "subnets_var_cidr" {
    default = "10.0.1.0/24"
}

variable "awsazs_var" {
	
	default = "eu-west-1a"
}


variable "webservers_var_ami" {
  default = "ami-0383535ce92966dfe"
}

variable "instance_var_type" {
  default = "t2.nano"
}

variable "security_var_group_ingress_rules" {
   
        default = [
        { 
          from_port   = 22
          to_port     = 22
          protocol    = "tcp"
          cidr_block  = ["0.0.0.0/0"]
          description = "SSHOnly"
        },
        {
          from_port   = 80
          to_port     = 80
          protocol    = "tcp"
          cidr_block  = ["0.0.0.0/0"]
          description = "Cli"
        },
    ]
}


variable "stream_name_var" {
}

variable "create_var_api_gateway" {
  default = false
}

variable "create_var_s3_backup" {
  default = true
}

variable "var_shard_count" {
  default = "1"
}

variable "var_retention_period" {
  default = "48"
}


output "invoke_url" {
  value = aws_api_gateway_deployment.mod[0].invoke_url
}

