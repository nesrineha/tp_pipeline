resource "aws_s3_bucket" "mod" {
  count = var.create_var_api_gateway
  bucket = "udacity-${var.stream_name_var}-event-backup"
  acl    = "private"
}

resource "aws_kinesis_firehose_delivery_stream" "mod" {
  name  = "${var.stream_name_var}-backup"
  count = var.create_var_api_gateway

  destination = "s3"

  s3_configuration {
    role_arn   = aws_iam_role.firehose_role[0].arn
    bucket_arn = aws_s3_bucket.mod[0].arn

    cloudwatch_logging_options {
      enabled         = "true"
      log_group_name  = aws_cloudwatch_log_group.mod[0].name
      log_stream_name_var = aws_cloudwatch_log_stream.mod[0].name
    }
  }
resource "aws_kinesis_stream" "mod" {
  name             = var.stream_name_var
  var_shard_count      = var.var_shard_count
  var_retention_period = var.var_retention_period

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
    "OutgoingRecords",
    "ReadProvisionedThroughputExceeded",
    "WriteProvisionedThroughputExceeded",
    "IncomingRecords",
    "IteratorAgeMilliseconds",
  ]

}

